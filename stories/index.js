import React from 'react';
import { storiesOf } from '@storybook/react';
import '../src/assets/styles/_general.scss';
import '../src/assets/styles/Button.scss';
import Spinner from '../src/components/Spinner/Spinner';

storiesOf('Button scss', module)
  .add('My button', () => (<button className="btn">My Button</button>))
  .add('com emoji', () => (<button className="btn active">My Button Active</button>));

storiesOf('Spinner', module)
  .add('Spinner for load on content', () => (<Spinner />))

storiesOf('Lists', module)
  .add('List', () => (
    <ul className="repos">
      <li>item 1</li>
      <li>item 2</li>
      <li>item 3</li>
      <li>item 4</li>
    </ul>
  ))
