## run project
git clone https://rafa88@bitbucket.org/rafa88/gympass-joao.git
enter the folder gympass-joao
run the command `npm install`
run the command `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### run the test
run the command `npm run test`

### Component Library `Storybook.js`
run the command `npm run storybook`
Open [http://localhost:9001](http://localhost:9001) to view it in the browser.

### Because I'm using
const/let

async/await
