import React, { Component } from 'react';
import { connect } from 'react-redux';

import '../App.scss';
import { fetchCommits, fetchCommitsUpdate } from '../actions/actions';
import HeaderComponent from '../components/Header/HeaderComponent';
import CommitComponent from '../components/Commit/CommitComponent';
import Spinner from '../components/Spinner/Spinner';

class Commits extends Component {

  componentDidMount() {
    this.props.dispatch(fetchCommits(this.props.match.params.id))

    window.addEventListener('scroll', this.onScroll.bind(this))
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll, false)
  }

  onScroll() {
    const scrollTop = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
    const scrollHeight = (document.documentElement && document.documentElement.scrollHeight) || document.body.scrollHeight;
    const clientHeight = document.documentElement.clientHeight || window.innerHeight;
    const scrolledToBottom = Math.ceil(scrollTop + clientHeight) >= scrollHeight;

    if (scrolledToBottom) {
      this.props.dispatch(fetchCommitsUpdate(this.props.match.params.id))
    }
  }

  render() {
    const {
      commits,
      loading,
      limit
    } = this.props;

    return (
      <div style={{ minHeight: '800px' }}>
        <HeaderComponent />
        {loading && (<Spinner />)}
        <div className="App" style={{ paddingTop: '10px' }}>
          <ul className="repos" ref="iScroll">
            {commits.map(function (commit, index) {
              return <li key={index}><CommitComponent commit={commit} /></li>
            })}
          </ul>
          <div className="no-more">
            {limit && 'No more results found!'}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  commits: store.commitState.commits,
  limit: store.commitState.limit,
  loading: store.commitState.loading
});

const CommitsController = connect(
  mapStateToProps
)(Commits)
export default CommitsController
// export default connect(mapStateToProps, null)(Commits);
