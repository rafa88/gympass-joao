import React, { Component } from 'react';
import { connect } from 'react-redux';

import '../App.scss';
import RepoItemComponent from '../components/RepoItem/RepoItemComponent';
import HeaderComponent from '../components/Header/HeaderComponent';
import Spinner from '../components/Spinner/Spinner';
import {
  toggleFilterStatus,
  fetchRepositories
} from "../actions/actions";

class Repositories extends Component {

  componentDidMount() {
    this.props.fetchRepositories()
  }

  render() {
    const {
      repos,
      loading,
      toggleFilterStatus,
      filters,
      error
    } = this.props;

    const orderRepositories = filters ? repos.sort((a, b) => a[filters] < b[filters] ? -1 : 1) : repos

    return (
      <div>
        <HeaderComponent />
        {loading && <Spinner />}
        {error ?
          <p id="ref_message_error" className="error">{error}</p>
          :
          <div className="App" style={{ paddingTop: '10px' }}>
            <ul className="order-by">
              <li><button className="btn" onClick={() => { toggleFilterStatus('name') }}>Order by name</button></li>
              <li><button className="btn" onClick={() => { toggleFilterStatus('updated_at') }}>Order by date</button></li>
              <li><button className="btn" onClick={() => { toggleFilterStatus('forks') }}>Order by forks</button></li>
            </ul>

            <ul className="repos">
              {orderRepositories.map(function (repo, index) {
                return <li key={index}><RepoItemComponent repo={repo} /></li>
              })}
            </ul>
          </div>
        }
      </div>
    )
  }
}

export const mapStateToProps = (store) => ({
  repos: store.repoState.repos,
  loading: store.repoState.loading,
  filters: store.repoFilter.order,
  error: store.repoState.error
})

const mapDispatchToProps = (dispatch) => ({
  fetchRepositories: () => {
    dispatch(fetchRepositories())
  },
  toggleFilterStatus: (statusName) => {
    dispatch(toggleFilterStatus(statusName))
  }
})

const RepositoriesController = connect(
  mapStateToProps,
  mapDispatchToProps
)(Repositories)
export default RepositoriesController
