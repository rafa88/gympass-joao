import React, { Component } from 'react';
import '../App.scss';

class NotFound extends Component {
  render() {
    return (
      <div className="App" style={{ paddingTop: '10px' }}>
        <h1>Pagina não encontrada</h1>
      </div>
    );
  }
}
export default NotFound;