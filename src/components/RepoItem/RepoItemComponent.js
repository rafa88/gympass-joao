import React from 'react';
import { Link } from 'react-router-dom'

import formatDate from '../../utils/formatDate';
import './RepoItemComponent.scss';

function RepoItemComponent(repo) {
  return (
    <div className="repo">
      <Link className="repo-name" to={"/commit/" + repo.repo.name}>{repo.repo.name}</Link>
      <p className="repo-description">{repo.repo.description}</p>
      <div className="repo-info">
        <p className="languages">{repo.repo.language}</p>
        <p className="updated">{formatDate(repo.repo.updated_at)}</p>
      </div>
    </div>
  )
}

export default RepoItemComponent;
