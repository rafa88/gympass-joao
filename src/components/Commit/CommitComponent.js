import React from 'react';

import formatDate from '../../utils/formatDate';
import './CommitComponent.scss';

function CommitComponent(commit) {
  return (
    <div className="commit">
      <p className="commit-description">{commit.commit.commit.message}</p>
      <div className="commit-info">
        <p className="commit-avatar"><img height="20" width="20" alt="@itzikya" src={commit.commit.author.avatar_url} /></p>
        <p className="commit-author">{commit.commit.author.login}</p>
        <p className="commit-time">committed <strong>{formatDate(commit.commit.commit.committer.date)}</strong></p>
      </div>
    </div>
  );
}

export default CommitComponent;
