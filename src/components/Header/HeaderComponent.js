import React from 'react';
import { Link } from 'react-router-dom'

import './HeaderComponent.scss';

function HeaderComponent() {
  return (
    <header className="header">
      <Link to="/" className="logo"></Link>
    </header>
  );
}

export default HeaderComponent;
