import React from 'react';

import './Spinner.scss';

function Spinner() {
  return (
    <div className="lds-dual-ring"></div>
  );
}

export default Spinner;
