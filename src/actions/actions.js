import {
  FETCH_REPOSITORIES,
  FETCH_REPOSITORIES_SUCCESS,
  FETCH_REPOSITORIES_ERROR,
  FETCH_COMMITS,
  FETCH_COMMITS_SUCCESS,
  FETCH_COMMITS_ERROR,
  FETCH_COMMITS_UPDATE,
  UPDATE_REPOSITORIES_SEARCH_STATUS, 
  FETCH_COMMITS_SUCCESS_LIMIT} from "../config/EventType";

export const fetchRepositories = () => async (dispatch, getState) => {
  dispatch({ type: FETCH_REPOSITORIES })
  await fetch("https://api.github.com/users/reactjs/repos")
    .then(handleErrors)
    .then(res => res.json())
    .then(json => dispatch({ type: FETCH_REPOSITORIES_SUCCESS, value: json }))
    .catch(error => dispatch({ type: FETCH_REPOSITORIES_ERROR, value: error.message }))
}

export function fetchCommits(id) {
  return dispatch => {
    dispatch({ type: FETCH_COMMITS });
    return fetch(`https://api.github.com/repos/reactjs/${id}/commits`)
      .then(handleErrors)
      .then(res => res.json())
      .then(json => dispatch({ type: FETCH_COMMITS_SUCCESS, value: json }) )
      .catch(error => dispatch({ type: FETCH_COMMITS_ERROR, repos: error.message }));
  };
}

export function fetchCommitsUpdate(id) {
  return (dispatch, getState) => {

    if (!getState().commitState.limit) {
      const page = getState().commitState.page + 1
      dispatch({ type: FETCH_COMMITS_UPDATE, value: page });

      return fetch(`https://api.github.com/repos/reactjs/${id}/commits?page=${page}`)
        .then(handleErrors)
        .then(res => res.json())
        .then(json => {
          if (json.length > 0) {
            dispatch({ type: FETCH_COMMITS_SUCCESS, value: json })
          } else {
            dispatch({type: FETCH_COMMITS_SUCCESS_LIMIT});
          }
          return json;
        })
        .catch(error => dispatch({ type: FETCH_COMMITS_ERROR, repos: error.message }));
    }
  };
}

export const toggleFilterStatus = statusName => (dispatch, getState) => {
  dispatch( { type: UPDATE_REPOSITORIES_SEARCH_STATUS, value: getState().repoFilter.order !== statusName ? statusName : '' } )
}

function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}
