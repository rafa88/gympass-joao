import { combineReducers } from 'redux';
import { repoReducer } from './repoReducer';
import { commitReducer } from './commitReducer';
import { repoFilterReducer } from './repoFilterReducer';

export const Reducers = combineReducers({
  repoFilter: repoFilterReducer,
  repoState: repoReducer,
  commitState: commitReducer,
});