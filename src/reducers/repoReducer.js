import { FETCH_REPOSITORIES, FETCH_REPOSITORIES_SUCCESS, FETCH_REPOSITORIES_ERROR } from "../config/EventType";

const initialState = {
  repos: [],
  loading: false,
  error: null
};

export const repoReducer = (state = initialState, action) => {

  switch (action.type) {
    case FETCH_REPOSITORIES:
      return {
        ...state,
        loading: true,
        error: null
      };
    case FETCH_REPOSITORIES_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        repos: action.value
      };
    case FETCH_REPOSITORIES_ERROR:
      return {
        ...state,
        loading: false,
        error: action.value
      };
    default:
      return state;
  }
}