import { FETCH_COMMITS, FETCH_COMMITS_SUCCESS, FETCH_COMMITS_ERROR, FETCH_COMMITS_UPDATE, FETCH_COMMITS_SUCCESS_LIMIT } from "../config/EventType";

const initialState = {
  commits: [],
  loading: true,
  error: null,
  page: 1,
  limit: false
};

export const commitReducer = (state = initialState, action) => {

  switch (action.type) {
    case FETCH_COMMITS:
      return {
        commits: [],
        page: 1,
        loading: true,
        error: null,
      };
    case FETCH_COMMITS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        commits: state.commits.concat(action.value)
      };
    case FETCH_COMMITS_SUCCESS_LIMIT:
      return {
        ...state,
        limit: true,
        loading: false
      };
    case FETCH_COMMITS_ERROR:
      return {
        ...state,
        loading: false,
        error: null,
        commits: action.value
      };
      case FETCH_COMMITS_UPDATE:
        return {
          ...state,
          loading: true,
          page: action.value
        };
      default:
      return state;
  }
}