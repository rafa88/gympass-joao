import { UPDATE_REPOSITORIES_SEARCH_STATUS } from "../config/EventType";

const initialState = {
  order: '',
}

export const repoFilterReducer = (state = initialState, action) => {

  switch (action.type) {
    case UPDATE_REPOSITORIES_SEARCH_STATUS:
      return {
        ...state,
        order: action.value
      };
    default:
      return state;
  }
}
