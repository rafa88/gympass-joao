import Enzyme, { mount, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';
import React from 'react'

const EnzymeSetup = (Component, defaultProps) => {
    Enzyme.configure({ adapter: new Adapter() });
    
    return (customProps, _shallow = false) => {
        const props = Object.assign({}, defaultProps, customProps)

        const fn = _shallow ? shallow : mount
        const enzymeWrapper = fn(<Component {...props} />)

        return {
            props,
            enzymeWrapper
        }
    }
}

export default EnzymeSetup