import { formatDate, isDate} from "../utils/formatDate";
// import { isDate } from "util";

describe('formatDate', () => {

	it('should create', () => {
		expect(formatDate).toBeTruthy();
	});

  it('should print days', () => {
    let value = formatDate('2019-05-22T18:42:41.988Z');
    expect(value).toContain('days');
	});

	it('should print hours', () => {
    let value = formatDate('2019-05-23T21:31:37Z');
    expect(value).toContain('hours');
  });

  it('isDate', () => {
    let value = isDate('2019-05-23T21:31:37Z');
    expect(value).toEqual(true);
  });

  it('date is not valid', () => {
    let value = isDate('asdsdsadsa');
    expect(value).toEqual(false);
  });

  it('date with timeStamp is not valid', () => {
    let value = isDate('1558647097000');
    expect(value).toEqual(false);
  });

});
