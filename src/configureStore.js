import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger'
import thunk from "redux-thunk";
import { Reducers } from './reducers';

export const configureStore = createStore(
  Reducers,
  applyMiddleware(logger, thunk)
);
