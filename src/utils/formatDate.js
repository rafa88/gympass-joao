export const formatDate = (value) => {
  if (!value) return false
  const date = new Date(value);
  const currentDate = new Date();
  const timeDiff = Math.abs(currentDate.getTime() - date.getTime());
  const diffHrs = Math.floor((timeDiff % 86400000) / 3600000);
  const diffMins = Math.round(((timeDiff % 86400000) % 3600000) / 60000);
  const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
  
  if (diffDays === 1 && diffHrs === 1) {
    return `${diffMins} min`
  } else if (diffDays === 1 && diffHrs > 1) {
    return `${diffHrs} hours`
  } else {
    return `${diffDays} days`
  }
};

export const isDate = (value) => {
  return !isNaN(Date.parse(value))
}

export default formatDate;
