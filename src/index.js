import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';

import { configureStore } from './configureStore';
import RepositoriesController from './screens/Repositories';
import CommitsController from './screens/Commits';
import NotFound from './screens/Not-found';

ReactDOM.render(
  <BrowserRouter>
    <Provider store={configureStore}>
      <Switch>
        <Route path="/" exact={true} component={RepositoriesController} />
        <Route path="/commit/:id" component={CommitsController} />
        <Route path='*' component={NotFound} />
      </Switch>
    </Provider>
  </BrowserRouter>, document.getElementById('root'));

serviceWorker.unregister();
